import API from './Api';

export const signUp = (params) => {
  const formData = new FormData();
  formData.append('first_name', params.first_name);
  formData.append('last_name', params.last_name);
  formData.append('username', params.username);
  formData.append('email', params.email);
  formData.append('password', params.password);
  formData.append('linkedin', params.linkedin);
  formData.append('linkedin_id', params.linkedin_id);
  return API.post('/register', formData);
};

export const signIn = (params) => {
  const formData = new FormData();
  formData.append('email', params.email);
  formData.append('password', params.password);
  formData.append('linkedin_id', params.linkedin_id);
  return API.post('/login', formData);
};

export const postPersonalInfo = (params) => {
  const formData = new FormData();
  formData.append('about', params.about);
  formData.append('date_of_birth', params.birthdate);
  formData.append('phone_number', params.phone_number);
  formData.append('professional_headline', params.professional_headline);
  formData.append('whatsapp', params.whatsapp);
  formData.append('street', params.street);
  formData.append('city', params.city);
  formData.append('state', params.state);
  formData.append('postal_code', params.postal_code);
  formData.append('country', params.country);
  formData.append('member_id', params.member_id);

  return API.post('/register/personal', formData);
};

export const postBusinessInfo = (params) => {

  const formData = new FormData();
  formData.append('member_id', params.member_id);
  formData.append('business_role', params.business_role);
  formData.append('business_stage', params.business_stage);
  formData.append('join_reason_growth[]', JSON.stringify(params.join_reason_growth));
  formData.append('join_reason_learn[]', JSON.stringify(params.join_reason_learn));
  formData.append('join_reason_other', params.join_reason_other);

  // formData.forEach(function(key, index){
  //   console.log(index,"::", key);
  // });

  return API.post('/register/business', formData);
};

export const postSelectClubInfo = (params) => {

  const formData = new FormData();
  formData.append('member_id', params.member_id);
  formData.append('club_ids', params.club_ids);

  return API.post('/register/clubs', formData);
};

export const getMemberProfileByIds = (params) => {
  console.log("******", params.ids);
  const formData = new FormData();
  params.ids.forEach((id, idx) => {
    formData.append(`member_ids[${idx}]`, id);
  })

  return API.post('/members/profiles', formData);
};

export const logout = () => {
  return API.post('/logout');
};

export const signInwithLinkedin = (params) => {
  const formData = new FormData();
  formData.append('code', params.code);
  return API.post('/linkedinAuth', formData);
};
export const signUpwithLinkedin = (params) => {
  const formData = new FormData();
  formData.append('code', params.code);
  return API.post('/signUpWithLinkedin', formData);
};