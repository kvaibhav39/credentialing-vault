import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Form, Row, Button, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
import { register } from "../redux/actions/userActions";
import FormContainer from "../components/FormContainer";

const RegisterScreen = ({ location, history }) => {
  const [accountFor, setAcountFor] = useState("doctor");
  // const [name, setName] = useState('');
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');
  // const [confirmPassword, setConfirmPassword] = useState('');
  // const [message, setMessage] = useState();

  // const dispatch = useDispatch();
  // const userRegister = useSelector(state => state.userRegister);

  // const { loading, userInfo, error } = userRegister;

  // const redirect = location.search ? location.search.split('=')[1] : '/';

  // useEffect(() => {
  //     if (userInfo) {
  //         history.push(redirect);
  //     }
  // }, [userInfo, history, redirect]);

  // const submitHandler = (e) => {
  //     e.preventDefault();
  //     if (password !== confirmPassword) {
  //         setMessage('Password do not Match');
  //     } else {
  //         dispatch(register(name, email, password));
  //     }

  // }

  return (
    <div className="create_account_step_one">
      <h2 className="create_account_heding">Create Account</h2>
      <p className="create_account_paregraph">
        Select your account type below.
      </p>
      <div className="create_account_category_section">
        <p className="create_account_category_top_text">I am a...</p>
        <div className="create_account_category_row">
          <a
            href="javascript:void(0)"
            className={`create_account_category_column ${
              accountFor === "doctor" && "active"
            }`}
            onClick={() => setAcountFor("doctor")}
          >
            <img
              className="create_account_category_img"
              src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
            />
            <h3 className="create_account_category_heading">Doctor</h3>
            <p className="create_account_category_price">$50/mo</p>
          </a>
          <a
            href="javascript:void(0)"
            className={`create_account_category_column ${
              accountFor === "org" && "active"
            }`}
            onClick={() => setAcountFor("org")}
          >
            <img
              className="create_account_category_img"
              src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
            />
            <h3 className="create_account_category_heading">Organization</h3>
            <p className="create_account_category_price">
              <span>Starting at</span> $100/mo
            </p>
          </a>
        </div>
        {accountFor === "doctor" ? (
          <Link to={"/doctor-account"} className="create_account_next_btn">
            Next
          </Link>
        ) : (
          <Link
            to={"/organization-account"}
            className="create_account_next_btn"
          >
            Next
          </Link>
        )}

        <Link to={"/"} className="create_account_back_btn">
          Back
        </Link>
      </div>
    </div>
  );
};

export default RegisterScreen;
