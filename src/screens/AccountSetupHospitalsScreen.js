import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";

const AccountSetupHospitalsScreen = ({ match }) => {
  const [appointed, setAppointed] = useState("");
  const [inputList, setInputList] = useState([
    {
      // user_email: getWithExpiry("userInfo").userInfo.email,
      name: "",
      address: "",
      city: "",
      state: "",
      postalcode: "",
      verificationcontact: "",
      verificationtelephone: "",
      verificationfax: "",
      verificationemail: "",
      startdate: "",
      enddate: "",
      speciality: "",
      status: "",
      letterofapproval: "",
      vdate: "",
      document: "",
    },
  ]);

  console.log("inputList", inputList);
  const handleInputChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      {
        // user_email: getWithExpiry("userInfo").userInfo.email,
        name: "",
        address: "",
        city: "",
        state: "",
        postalcode: "",
        verificationcontact: "",
        verificationtelephone: "",
        verificationfax: "",
        verificationemail: "",
        startdate: "",
        enddate: "",
        speciality: "",
        status: "",
        letterofapproval: "",
        vdate: "",
        document: "",
      },
    ]);
  };
  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Board Certifcations"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Hospitals"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check type="checkbox" label="Work History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Hospitals" />
              <p>
                Have you ever been appointed affiliation or privileges not
                associated with your education or employment at a hospital?
              </p>
              <div className="heading_radio_btn custom_radio_btn_content">
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="yes"
                    type="radio"
                    name="preferredContact"
                    value="yes"
                    onChange={(e) => setAppointed(e.target.value)}
                    checked={appointed === "yes"}
                  />
                  <label for="yes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="no"
                    type="radio"
                    name="preferredContact"
                    value="no"
                    onChange={(e) => setAppointed(e.target.value)}
                    checked={appointed === "no"}
                  />
                  <label for="no">No</label>
                </div>
              </div>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">
                List all hospital affiliations or privileges that you have (not
                associated with training). Medical staff services address,
                telephone, fax, email and contact are required.
              </p>
              {inputList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                          type="text"
                          name="name"
                          value={x.name}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Address:</Form.Label>
                        <Form.Control
                          type="text"
                          name="address"
                          value={x.address}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={5}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>City:</Form.Label>
                        <Form.Control
                          type="text"
                          name="city"
                          value={x.city}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={3}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>State:</label>
                        <select
                          name="state"
                          value={x.state}
                          onChange={(e) => handleInputChange(e, i)}
                        >
                          <option>Choose</option>
                          <option>value</option>
                        </select>
                      </Form.Group>
                    </Col>
                    <Col lg={4}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Postal Code:</Form.Label>
                        <Form.Control
                          type="text"
                          name="postalcode"
                          value={x.postalcode}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Contact:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationcontact"
                          value={x.verificationcontact}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Telephone:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationtelephone"
                          value={x.verificationtelephone}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Fax:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationfax"
                          value={x.verificationfax}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Email:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationemail"
                          value={x.verificationemail}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Start Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="startdate"
                          value={x.startdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>End Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="enddate"
                          value={x.enddate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Speciality:</Form.Label>
                        <Form.Control
                          type="text"
                          name="speciality"
                          value={x.speciality}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Status:</Form.Label>
                        <Form.Control
                          type="text"
                          name="status"
                          value={x.status}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>
                          Letter of Approval of Privileges:
                        </Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="letterofapproval"
                            value={x.letterofapproval}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="vdate"
                          value={x.vdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Documentation:</Form.Label>
                        <Form.Control
                          type="text"
                          name="document"
                          value={x.document}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button className="pluse_add_text_btn" onClick={handleAddClick}>
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
                Add
              </Button>
              <div className="next_back_btns">
                <Link
                  to={"/account-setup-board-certifications"}
                  className="back_btn"
                >
                  Back
                </Link>
                <Link to={"/account-setup-work-history"} className="next_btn">
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupHospitalsScreen;
