import React, { useState, useEffect } from "react";
import { LinkContainer } from "react-router-bootstrap";
import { Form, Row, Button, Col, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
// import { getUserDetails, updateUserProfile } from '../redux/actions/userActions';
import { USER_UPDATE_PROFILE_RESET } from "../redux/actions/types";

const ProfileScreen = ({ location, history }) => {
  //   const [name, setName] = useState("");
  //   const [email, setEmail] = useState("");
  //   const [password, setPassword] = useState("");
  //   const [confirmPassword, setConfirmPassword] = useState("");
  //   const [message, setMessage] = useState();
  //   const dispatch = useDispatch();
  //   const userDetails = useSelector((state) => state.userDetails);
  //   const { loading, user, error } = userDetails;
  //   const userLogin = useSelector((state) => state.userLogin);
  //   const { userInfo } = userLogin;
  //   const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
  //   const { success } = userUpdateProfile;
  //   const orderListMy = useSelector((state) => state.orderListMy);
  //   const { loading: loadingOrders, error: errorOrders, orders } = orderListMy;
  //   useEffect(() => {
  //     if (!userInfo) {
  //       history.push("/login");
  //     } else {
  //       if (!user || !user.name || success) {
  //         dispatch({ type: USER_UPDATE_PROFILE_RESET });
  //         dispatch(getUserDetails("profile"));
  //       } else {
  //         setName(user.name);
  //         setEmail(user.email);
  //       }
  //     }
  //   }, [dispatch, userInfo, history, user, success]);
  //   const submitHandler = (e) => {
  //     e.preventDefault();
  //     if (password !== confirmPassword) {
  //       setMessage("Password do not Match");
  //     } else {
  //       dispatch(updateUserProfile({ id: user._id, email, password, name }));
  //     }
  //   };
  //   return (
  //     <Row>
  //       <Col md={3}>
  //         <h2>User Profile</h2>
  //         {error ? <Message variant="danger">{error}</Message> : null}
  //         {loading ? <Loader /> : null}
  //         {message ? <Message variant="danger">{message}</Message> : null}
  //         {success ? (
  //           <Message variant="success">{"Profile Updated"}</Message>
  //         ) : null}
  //         <Form onSubmit={submitHandler}>
  //           <Form.Group controlId="name">
  //             <Form.Label>Name</Form.Label>
  //             <Form.Control
  //               type="text"
  //               value={name}
  //               placeholder="Enter Name"
  //               onChange={(e) => setName(e.target.value)}
  //             ></Form.Control>
  //           </Form.Group>
  //           <Form.Group controlId="email">
  //             <Form.Label>Email</Form.Label>
  //             <Form.Control
  //               type="email"
  //               value={email}
  //               placeholder="email@gmail.com"
  //               onChange={(e) => setEmail(e.target.value)}
  //             ></Form.Control>
  //           </Form.Group>
  //           <Form.Group controlId="password">
  //             <Form.Label>Password</Form.Label>
  //             <Form.Control
  //               type="password"
  //               value={password}
  //               placeholder="**********"
  //               onChange={(e) => setPassword(e.target.value)}
  //             ></Form.Control>
  //           </Form.Group>
  //           <Form.Group controlId="confirmPassword">
  //             <Form.Label>Confirm Password</Form.Label>
  //             <Form.Control
  //               type="password"
  //               value={confirmPassword}
  //               placeholder="**********"
  //               onChange={(e) => setConfirmPassword(e.target.value)}
  //             ></Form.Control>
  //           </Form.Group>
  //           <Button type="submit" variant="primary">
  //             Update
  //           </Button>
  //         </Form>
  //       </Col>
  //       <Col md={9}>
  //         <h2>My Order</h2>
  //         {loadingOrders ? (
  //           <Loader />
  //         ) : errorOrders ? (
  //           <Message variant="danger">{errorOrders}</Message>
  //         ) : (
  //           <Table striped bordered hover responsive className="table-sm">
  //             <thead>
  //               <tr>
  //                 <th>ID</th>
  //                 <th>DATE</th>
  //                 <th>TOTAL</th>
  //                 <th>PAID</th>
  //                 <th>DELIVERED</th>
  //                 <th></th>
  //               </tr>
  //             </thead>
  //             <tbody>
  //               {orders.map((order) => (
  //                 <tr key={order._id}>
  //                   <td>{order._id}</td>
  //                   <td>{order.createdAt.substring(0, 10)}</td>
  //                   <td>{order.totalPrice}</td>
  //                   <td>
  //                     {order.isPaid ? (
  //                       order.paidAt.substring(0, 10)
  //                     ) : (
  //                       <i className="fas fa-times" style={{ color: "red" }}></i>
  //                     )}
  //                   </td>
  //                   <td>
  //                     {order.isDelivered ? (
  //                       order.DeliveredAt.substring(0, 10)
  //                     ) : (
  //                       <i className="fas fa-times" style={{ color: "red" }}></i>
  //                     )}
  //                   </td>
  //                   <td>
  //                     <LinkContainer to={`/order/${order._id}`}>
  //                       <Button className="btn-sm" variant="light">
  //                         Details
  //                       </Button>
  //                     </LinkContainer>
  //                   </td>
  //                 </tr>
  //               ))}
  //             </tbody>
  //           </Table>
  //         )}
  //       </Col>
  //     </Row>
  //   );
};

export default ProfileScreen;
