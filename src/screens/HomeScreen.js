import React from "react";
import authClient from "../sdk/api";
import HandleRegister from "../components/HandleRegister";
// import PropTypes from 'prop-types';
import _ from "lodash";
import config from "../config";
import { Link, Redirect } from "react-router-dom";
import { Tabs, Tab, Row, Form, Col } from "react-bootstrap";
import whiteLogo from "../assets/images/white-logo.svg";
import { getWithExpiry } from "../config/helper";

/**
 * React component for managing the return entry point of the implicit OAuth 2.0 flow and is expecting "access_token", "id_token" or "code" in a redirect uri.
 * The user will be redirected to this point based on the redirect_uri in config.js - the URL that specifies the return entry point of this application.
 */
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      access_token: null,
      id_token: null,
      idTokenJson: null,
      userInfo: null,
      errorMessage: "",
    };

    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleSignOff = this.handleSignOff.bind(this);
  }

  async handleSignIn() {
    this.clearSession();
    let state = authClient.generateRandomValue();
    let nonce = authClient.generateRandomValue();
    // Store state and nonce parameters into the session, so we can retrieve them after
    // user will be redirected back with access token or code (since react state is cleared in this case)
    await localStorage.setItem(
      "state",
      JSON.stringify({
        state,
        expiry: new Date().getTime() + 1000 * 60 * 60,
      })
    );
    await localStorage.setItem(
      "nonce",
      JSON.stringify({
        nonce,
        expiry: new Date().getTime() + 1000 * 60 * 60,
      })
    );

    authClient.authorize(state, nonce);
  }

  handleSignOff() {
    if (getWithExpiry("id_token").id_token) {
      authClient.signOff(
        getWithExpiry("id_token").id_token,
        getWithExpiry("state").state
      );
      this.clearSession();
    }
  }

  clearSession() {
    this.setState({
      access_token: null,
      id_token: null,
      errorMessage: "",
    });
    localStorage.removeItem("userInfo");
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("state");
    localStorage.removeItem("nonce");
  }

  componentDidMount() {
    const hashes = authClient.parseHash();
    if (hashes.error && hashes.error_description) {
      this.setState({
        errorMessage: hashes.error + ": " + hashes.error_description,
      });
      return;
    }

    const stateMatch = window.location.href.match("[?#&]state=([^&]*)");
    if (
      stateMatch &&
      !stateMatch[1] &&
      !_.isEqual(stateMatch[1], getWithExpiry("state").state)
    ) {
      this.setState({
        errorMessage: "State parameter mismatch. ",
      });
      this.clearSession();
      return;
    }

    const codeMatch = window.location.href.match("[?#&]code=([^&]*)");
    // Implicit flow: access token is present in URL
    if (hashes.access_token) {
      this.setState({
        access_token: hashes.access_token,
        id_token: hashes.id_token,
      });
      localStorage.setItem(
        "access_token",
        JSON.stringify({
          access_token: hashes.access_token,
          expiry: new Date().getTime() + 1000 * 60 * 60,
        })
      );
      localStorage.setItem(
        "id_token",
        JSON.stringify({
          id_token: hashes.id_token,
          expiry: new Date().getTime() + 1000 * 60 * 60,
        })
      );
      this.handleUserInfo(hashes.access_token);
    }
    // Authorization code flow: access code is present in URL
    else if (codeMatch && codeMatch[1]) {
      authClient
        .getAccessToken(codeMatch[1])
        .then((token) => {
          this.setState({
            access_token: token.access_token,
            id_token: token.id_token,
          });
          localStorage.setItem(
            "access_token",
            JSON.stringify({
              access_token: token.access_token,
              expiry: new Date().getTime() + 1000 * 60 * 60,
            })
          );
          localStorage.setItem(
            "id_token",
            JSON.stringify({
              id_token: token.id_token,
              expiry: new Date().getTime() + 1000 * 60 * 60,
            })
          );
          this.handleUserInfo(token.access_token);
          this.verifyToken(token.id_token);
        })
        .catch((error) => {
          this.setState({
            errorMessage:
              "Couldn't get an access token. " +
              _.get(error, "error_description", _.get(error, "message", "")),
          });
        });
    }

    if (hashes.id_token) {
      this.verifyToken(hashes.id_token);
      localStorage.setItem(
        "id_token",
        JSON.stringify({
          id_token: hashes.id_token,
          expiry: new Date().getTime() + 1000 * 60 * 60,
        })
      );
    }
    // Replace current URL without adding it to history entries
    window.history.replaceState({}, "", "/");
  }

  verifyToken(id_token) {
    authClient
      .verifyIdToken(id_token, {
        nonce: getWithExpiry("nonce").nonce,
        maxAge: config.maxAge,
      })
      .then((idToken) => {
        this.setState({
          idTokenJson: idToken,
        });
        localStorage.setItem(
          "id_token_json",
          JSON.stringify({
            id_token_json: idToken,
            expiry: new Date().getTime() + 1000 * 60 * 60,
          })
        );
      })
      .catch((error) => {
        this.setState({
          errorMessage:
            "Id token verification failed. " +
            _.get(error, "error_description", _.get(error, "message", error)),
        });
      });
  }

  handleUserInfo(access_token) {
    authClient
      .getUserInfo(access_token)
      .then((result) => {
        this.setState({
          userInfo: result,
        });

        localStorage.setItem(
          "userInfo",
          JSON.stringify({
            userInfo: result,
            expiry: new Date().getTime() + 1000 * 60 * 60,
          })
        );
      })
      .catch((error) => {
        const errorDetail = _.get(error, "details[0].code", null);
        if (_.isEqual(errorDetail, "INVALID_VALUE")) {
          if (
            _.get(error, "details[0].message", null).includes(
              "Access token expired"
            )
          ) {
            this.setState({
              errorMessage: "Your access token is expired. Please login again.",
            });
          } else {
            this.setState({
              errorMessage: _.get(error, "details[0].message", null),
            });
          }
        } else if (errorDetail) {
          this.setState({
            errorMessage:
              errorDetail + _.get(error, "details[0].message", null),
          });
        } else if (
          _.get(error, "error", null) ||
          _.get(error, "error_description", null)
        ) {
          this.setState({
            errorMessage:
              _.get(error, "error", null) +
              ": " +
              _.get(error, "error_description", null),
          });
        }
        return Promise.reject(error);
      });
  }

  render() {
    const { errorMessage } = this.state;
    const alert = errorMessage && (
      <div className="alert alert-danger">{errorMessage}</div>
    );

    const content = getWithExpiry("access_token") ? (
      //   <Redirect to="/dashboard" />
      <HandleRegister />
    ) : (
      <>
        <div className="login_home_page_section">
          <Row className="align-items-center m-0">
            <Col md={6} className="login_home_page_left_column">
              <img
                src={whiteLogo}
                alt="white logo"
                className="login_home_page_logo"
              />
            </Col>
            <Col md={6} className="login_home_page_right_column">
              <Tabs
                defaultActiveKey="doctor"
                id="uncontrolled-tab-example"
                className="mb-3"
              >
                <Tab eventKey="doctor" title="Doctor">
                  <Form className="login_home_page_right_data">
                    <h2>Login to your account</h2>
                    <p>Please enter your email and password to continue.</p>
                    {/* <Link to={'/register'} className="login_home_page_right_login_btn">Login</Link> */}
                    <a
                      type="button"
                      className="login_home_page_right_login_btn"
                      onClick={this.handleSignIn}
                    >
                      Login
                    </a>
                    <Form.Group controlId="formBasicCheckbox">
                      <Form.Check type="checkbox" label="Remember me" />
                    </Form.Group>
                    <div className="login_home_page_right_bottom_link">
                      <p>
                        Don't have and account? <a>Sign Up</a>
                      </p>
                      <Link
                        to="#"
                        className="login_home_page_right_forgot_link"
                      >
                        Forgot password?
                      </Link>
                    </div>
                  </Form>
                </Tab>
                <Tab eventKey="organization" title="Organization">
                  <Form className="login_home_page_right_data">
                    <h2>Login to your account</h2>
                    <p>Please enter your email and password to continue.</p>
                    {/* <Link to={'/register'} className="login_home_page_right_login_btn">Login</Link> */}
                    <a
                      type="button"
                      className="login_home_page_right_login_btn"
                      onClick={this.handleSignIn}
                    >
                      Login
                    </a>
                    <Form.Group controlId="formBasicCheckbox">
                      <Form.Check type="checkbox" label="Remember me" />
                    </Form.Group>
                    <div className="login_home_page_right_bottom_link">
                      <p>
                        Don't have and account? <a>Sign Up</a>
                      </p>
                      <Link
                        to="#"
                        className="login_home_page_right_forgot_link"
                      >
                        Forgot password?
                      </Link>
                    </div>
                  </Form>
                </Tab>
              </Tabs>
            </Col>
          </Row>
        </div>
      </>
    );

    return (
      <div className="container">
        {alert}
        {content}
      </div>
    );
  }
}

// Home.propTypes = {
// 	environmentId: PropTypes.string.isRequired,
// 	clientId: PropTypes.string.isRequired,
// 	clientSecret: PropTypes.string,
// 	scope: PropTypes.string.isRequired,
// 	responseType: PropTypes.string,
// 	tokenEndpointAuthMethod: PropTypes.string.isRequired,
// 	grantType: PropTypes.string,
// 	prompt: PropTypes.string,
// 	redirectUri: PropTypes.string,
// 	logoutRedirectUri: PropTypes.string,
// 	maxAge: PropTypes.number
// };

export default Home;
