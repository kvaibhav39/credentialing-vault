import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";
import { getWithExpiry } from "../config/helper";

const AccountSetupLicensesCredentialsScreen = ({ match }) => {
  const [inputList, setInputList] = useState([
    {
      user_email: getWithExpiry("userInfo").userInfo.email,
      type: "",
      name: "",
      address1: "",
      address2: "",
      state: "",
      postalcode: "",
      country: "",
      programdirector: "",
      telephone: "",
      fax: "",
      email: "",
      sdate: "",
      edate: "",
      subject: "",
      vdate: "",
      document: "",
      diploma_certificate: "",
      english_diploma_certificate: "",
    },
  ]);

  console.log("inputList", inputList);
  const handleInputChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      {
        user_email: getWithExpiry("userInfo").userInfo.email,
        type: "",
        name: "",
        address1: "",
        address2: "",
        state: "",
        postalcode: "",
        country: "",
        programdirector: "",
        telephone: "",
        fax: "",
        email: "",
        sdate: "",
        edate: "",
        subject: "",
        vdate: "",
        document: "",
        diploma_certificate: "",
        english_diploma_certificate: "",
      },
    ]);
  };
  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check type="checkbox" label="Board Certifcations" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check type="checkbox" label="Hospitals" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check type="checkbox" label="Work History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Foreign Medical Graduation" />
              <p>Are you a foreign Medical Graduate?</p>
              <div className="heading_radio_btn custom_radio_btn_content">
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="yes" type="radio" name="preferredContact" />
                  <label for="yes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="no" type="radio" name="preferredContact" />
                  <label for="no">No</label>
                </div>
              </div>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">
                Enter your ECFMG information.
              </p>
              {inputList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Type:</Form.Label>
                        <Form.Control
                          type="text"
                          name="type"
                          value={x.value}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                          type="text"
                          name="name"
                          value={x.name}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Address 1</Form.Label>
                        <Form.Control
                          type="text"
                          name="address1"
                          value={x.address1}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Address 2</Form.Label>
                        <Form.Control
                          type="text"
                          name="address2"
                          value={x.address2}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={8}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>State / Province:</label>
                        <select
                          name="state"
                          value={x.state}
                          onChange={(e) => handleInputChange(e, i)}
                        >
                          <option>Choose</option>
                          <option>value</option>
                        </select>
                      </Form.Group>
                    </Col>
                    <Col lg={4}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Postal Code:</Form.Label>
                        <Form.Control
                          type="text"
                          name="postalcode"
                          value={x.postalcode}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>Country:</label>
                        <select
                          name="country"
                          value={x.country}
                          onChange={(e) => handleInputChange(e, i)}
                        >
                          <option>Choose</option>
                          <option>value</option>
                        </select>
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Program Director:</Form.Label>
                        <Form.Control
                          type="text"
                          name="programdirector"
                          value={x.programdirector}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Program Director Telephone:</Form.Label>
                        <Form.Control
                          type="text"
                          name="telephone"
                          value={x.telephone}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Program Director Fax:</Form.Label>
                        <Form.Control
                          type="text"
                          name="fax"
                          value={x.fax}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Program Director Email:</Form.Label>
                        <Form.Control
                          type="email"
                          name="email"
                          value={x.email}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Start Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="sdate"
                          value={x.sdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>End Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="edate"
                          value={x.edate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Degree or Subject Earned:</Form.Label>
                        <Form.Control
                          type="text"
                          name="subject"
                          value={x.subject}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Diploma or Certificate:</Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="diploma_certificate"
                            value={x.diploma_certificate}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>
                          English Diploma or Certificate Translation(if
                          applicable):
                        </Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="english_diploma_certificate"
                            value={x.english_diploma_certificate}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="vdate"
                          value={x.vdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Documentation:</Form.Label>
                        <Form.Control
                          type="text"
                          name="document"
                          value={x.document}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button className="pluse_add_text_btn" onClick={handleAddClick}>
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
                Add
              </Button>
              <div className="next_back_btns">
                <Link
                  to={"/account-setup-education-training"}
                  className="back_btn"
                >
                  Back
                </Link>
                <Link
                  to={"/account-setup-licenses-credentials"}
                  className="next_btn"
                >
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupLicensesCredentialsScreen;
