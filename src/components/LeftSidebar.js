import React from 'react';
import { Link } from 'react-router-dom';

const LeftSidebar = () => {
    return (
        <>
            <input type="checkbox" id="leftSidebarToggelInput" />
            <div className="comman_left_sidebar_section">
                <div className="comman_left_sidebar_content">
                <label for="leftSidebarToggelInput" className="left_sidebar_toggel_btn">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                </svg>
                </label>
                    <ul className="left_siderbar_nav">
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link active">
                                Dashboard
                            </Link>
                        </li>
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link">
                                Action Center
                            </Link>
                        </li>
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link">
                                Audit Trail
                            </Link>
                        </li>
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link">
                                Users
                            </Link>
                        </li>
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link">
                                Document Signature
                            </Link>
                        </li>
                    </ul>
                    <ul className="left_siderbar_nav left_siderbar_support_policy_nav">
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link active">
                                Support
                            </Link>
                        </li>
                        <li className="left_sidebar_items">
                            <Link to={'/dashboard'} className="left_sidebar_link">
                                Privacy & Policy
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default LeftSidebar
