import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";

const AccountSetupWorkHistoryScreen = ({ match }) => {
  const [inputList, setInputList] = useState([
    {
      // user_email: getWithExpiry("userInfo").userInfo.email,
      name: "",
      type: "",
      position: "",
      currentposition: "",
      startdate: "",
      enddate: "",
      address1: "",
      address2: "",
      city: "",
      state: "",
      postalcode: "",
      verificationcontact: "",
      verificationtelephone: "",
      verificationfax: "",
      verificationemail: "",
      coverpage: "",
      vdate: "",
      document: "",
    },
  ]);

  console.log("inputList", inputList);
  const handleInputChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      {
        // user_email: getWithExpiry("userInfo").userInfo.email,
        name: "",
        type: "",
        position: "",
        currentposition: "",
        startdate: "",
        enddate: "",
        address1: "",
        address2: "",
        city: "",
        state: "",
        postalcode: "",
        verificationcontact: "",
        verificationtelephone: "",
        verificationfax: "",
        verificationemail: "",
        coverpage: "",
        vdate: "",
        document: "",
      },
    ]);
  };

  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Board Certifcations"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Hospitals"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Work History"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Work History" />
              <p>
                All contact information entered must be HR type contacts able to
                verify your employment. This is not necessarily your supervisor
                but could be. Please verify with listed contact before
                submitting information to be sure it is the person you should
                list.
              </p>
              <p>
                If you are an MD, add all employments since you started medical
                school.
              </p>
              <p>
                If you are an NP, list all employment since you graduated as an
                RN.
              </p>
              <p>
                If you are a PA, list all employment since undergraduate school.
                If it has been less than 5 years since you graduated, provide 5
                years of employment.
              </p>
              <p>
                If you are a PT/OT, list all employments since you completed
                your undergraduate. If it has been less than 5 years since you
                graduated, provide 5 years of employment.
              </p>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              {inputList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                          type="text"
                          name="name"
                          value={x.name}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Type:</Form.Label>
                        <Form.Control
                          type="text"
                          name="type"
                          value={x.type}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Position:</Form.Label>
                        <Form.Control
                          type="text"
                          name="position"
                          value={x.position}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Current Position:</Form.Label>
                        <div className="custom_radio_btn_content">
                          <div className="custom_radio_btn">
                            <input
                              id="yes"
                              type="radio"
                              name="currentposition"
                              value="yes"
                              onChange={(e) => handleInputChange(e, i)}
                              checked={x.currentposition === "yes"}
                            />
                            <label for="yes">Yes</label>
                          </div>
                          <div className="custom_radio_btn">
                            <input
                              id="no"
                              type="radio"
                              name="currentposition"
                              value="no"
                              onChange={(e) => handleInputChange(e, i)}
                              checked={x.currentposition === "no"}
                            />
                            <label for="no">No</label>
                          </div>
                        </div>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Start Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="startdate"
                          value={x.startdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>End Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="enddate"
                          value={x.enddate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Address 1:</Form.Label>
                        <Form.Control
                          type="text"
                          name="address1"
                          value={x.address1}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Address 2:</Form.Label>
                        <Form.Control
                          type="text"
                          name="address2"
                          value={x.address2}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={5}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>City:</Form.Label>
                        <Form.Control
                          type="text"
                          name="city"
                          value={x.city}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={3}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>State:</label>
                        <select
                          name="state"
                          value={x.state}
                          onChange={(e) => handleInputChange(e, i)}
                        >
                          <option>Choose</option>
                          <option>value</option>
                        </select>
                      </Form.Group>
                    </Col>
                    <Col lg={4}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Postal Code:</Form.Label>
                        <Form.Control
                          type="text"
                          name="postalcode"
                          value={x.postalcode}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Contact:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationcontact"
                          value={x.verificationcontact}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Telephone:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationtelephone"
                          value={x.verificationtelephone}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Fax:</Form.Label>
                        <Form.Control
                          type="text"
                          name="verificationfax"
                          value={x.verificationfax}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verification Email:</Form.Label>
                        <Form.Control
                          type="email"
                          name="verificationemail"
                          value={x.verificationemail}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Malpractice Cover Page:</Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="coverpage"
                            value={x.coverpage}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="vdate"
                          value={x.vdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Documentation:</Form.Label>
                        <Form.Control
                          type="text"
                          name="document"
                          value={x.document}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button className="pluse_add_text_btn" onClick={handleAddClick}>
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
                Add
              </Button>
              <div className="next_back_btns">
                <Link to={"/account-setup-hospitals"} className="back_btn">
                  Back
                </Link>
                <Link to={"/account-setup-gap"} className="next_btn">
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupWorkHistoryScreen;
