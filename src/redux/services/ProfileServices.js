import API from './Api';

export const getProfileData = (params) => {
    return API.get(`/my/account/${params.id}`);
};

export const getProfileDataForChat = (params) => {
    return API.get(`/my/account/${params.id}`);
};

export const updatePersonalInfo = (params) => {
    const formData = new FormData();
    formData.append('first_name', params.first_name);
    formData.append('last_name', params.last_name);
    formData.append('phone_number', params.phone_number);
    formData.append('professional_headline', params.professional_headline);
    // formData.forEach(function(key, index){
    //     console.log(index,"::", key);
    // });
    return API.post(`/my/account/${params.id}/personal`, formData);
};

export const updateBiography = (params) => {
    const formData = new FormData();
    formData.append('about', params.about);

    return API.post(`/my/account/${params.id}/biography`, formData);
};

export const updateSocial = (params) => {
    const formData = new FormData();
    formData.append('linkedin', params.linkedin);
    formData.append('facebook', params.facebook);
    formData.append('twitter', params.twitter);

    return API.post(`/my/account/${params.id}/social`, formData);
};

export const updateProfileImage = (params) => {
    const formData = new FormData();
    formData.append('profile', params.profile);

    return API.post(`/my/account/${params.id}/profile`, formData);
};

export const updateCoverImage = (params) => {
    const formData = new FormData();
    formData.append('cover', params.cover);

    return API.post(`/my/account/${params.id}/cover`, formData);
};

export const updateAccount = (params) => {
    const formData = new FormData();
    formData.append('username', params.username);
    formData.append('email', params.email);

    return API.post(`/my/account/${params.id}/update`, formData);
};

export const updatePassword = (params) => {
    const formData = new FormData();
    formData.append('password', params.password);
    formData.append('new_password', params.new_password);

    return API.post(`/my/account/${params.id}/changePassword`, formData);
};

export const updateSetting = (params) => {
    const formData = new FormData();
    if (params.privacy_follow_me !== undefined) {
        formData.append('privacy_follow_me', params.privacy_follow_me);
    }
    if (params.privacy_view_profile !== undefined) {
        formData.append('privacy_view_profile', params.privacy_view_profile);
    }
    if (params.notification_follow !== undefined) {
        formData.append('notification_follow', params.notification_follow);
    }
    if (params.notification_contact_preferred1) {
        formData.append('notification_contact_preferred1', params.notification_contact_preferred1);
    }
    if (params.notification_contact_preferred2) {
        formData.append('notification_contact_preferred2', params.notification_contact_preferred2);
    }
    if (params.notification_contact_preferred3) {
        formData.append('notification_contact_preferred3', params.notification_contact_preferred3);
    }
    if (params.notification_like_comment_post !== undefined) {
        formData.append('notification_like_comment_post', params.notification_like_comment_post);
    }

    // formData.forEach(function(key, index){
    //     console.log("formData",index,"::", key);
    // });

    return API.post(`/my/account/${params.id}/settings`, formData);
};

export const deleteAccount = (params) => {
    return API.del(`/account/${params.id}?category=MEMBER`);
};