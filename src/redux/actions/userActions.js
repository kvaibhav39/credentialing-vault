// import {
//     USER_LOGIN_REQUEST, USER_LOGOUT, USER_LOGIN_SUCCESS, USER_LOGIN_FAIL,
//     USER_REGISTER_SUCCESS, USER_REGISTER_REQUEST, USER_REGISTER_FAIL,
//     USER_DETAILS_REQUEST, USER_DETAILS_SUCCESS, USER_DETAILS_FAIL,
//     USER_UPDATE_PROFILE_REQUEST, USER_UPDATE_PROFILE_SUCCESS,
//     USER_UPDATE_PROFILE_FAIL, USER_DETAILS_RESET,
//     USER_LIST_REQUEST, USER_LIST_SUCCESS, USER_LIST_FAIL, USER_LIST_RESET,
//     USER_DELETE_REQUEST, USER_DELETE_SUCCESS, USER_DELETE_FAIL,
//     USER_UPDATE_FAIL, USER_UPDATE_SUCCESS, USER_UPDATE_REQUEST
// } from './types';
// // import Api from '../services/Api';
// import axios from 'axios';

// export const login = (email, password) => {
// return async (dispatch) => {
//     try {
//         dispatch({ type: USER_LOGIN_REQUEST });
//         const config = {
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         }
//         const { data } = await axios.post('/api/users/login', { email, password }, config);
//         // const data = await Api.post('/api/users/login', { email, password });
//         dispatch({
//             type: USER_LOGIN_SUCCESS,
//             payload: data
//         })
//         localStorage.setItem('userInfo', JSON.stringify(data))

//     } catch (error) {
//         dispatch({
//             type: USER_LOGIN_FAIL,
//             payload: error.response && error.response.data.message ? error.response.data.message : error.message
//         })
//     }

// }
// }

// export const logout = () => {
//     return (dispatch) => {
//         localStorage.removeItem('userInfo')
//         localStorage.removeItem('cartItems')
//         localStorage.removeItem('shippingAddress')
//         localStorage.removeItem('paymentMethod')
//         dispatch({ type: USER_LOGOUT });
//         dispatch({ type: USER_DETAILS_RESET });
//         dispatch({ type: USER_LIST_RESET });
//     }
// }

// export const register = (name, email, password) => {
//     return async (dispatch) => {
//         try {
//             dispatch({ type: USER_REGISTER_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json'
//                 }
//             }
//             const { data } = await axios.post('/api/users', { name, email, password }, config);

//             dispatch({
//                 type: USER_REGISTER_SUCCESS,
//                 payload: data
//             });
//             dispatch({
//                 type: USER_LOGIN_SUCCESS,
//                 payload: data
//             })
//             localStorage.setItem('userInfo', JSON.stringify(data))

//         } catch (error) {
//             dispatch({
//                 type: USER_REGISTER_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }

//     }
// }

// export const getUserDetails = (id) => {
//     return async (dispatch, getState) => {
//         try {
//             dispatch({ type: USER_DETAILS_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${getState().userLogin.userInfo.token}`
//                 }
//             }
//             const { data } = await axios.get(`/api/users/${id}`, config);
//             dispatch({
//                 type: USER_DETAILS_SUCCESS,
//                 payload: data
//             })
//         } catch (error) {
//             dispatch({
//                 type: USER_DETAILS_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }
//     }
// }

// export const updateUserProfile = (user) => {
//     return async (dispatch, getState) => {
//         try {
//             dispatch({ type: USER_UPDATE_PROFILE_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${getState().userLogin.userInfo.token}`
//                 }
//             }
//             const { data } = await axios.put(`/api/users/profile`, user, config);
//             dispatch({
//                 type: USER_UPDATE_PROFILE_SUCCESS,
//                 payload: data
//             })

//             dispatch({
//                 type: USER_LOGIN_SUCCESS,
//                 payload: data
//             });
//             localStorage.setItem('userInfo', JSON.stringify(data));
//         } catch (error) {
//             dispatch({
//                 type: USER_UPDATE_PROFILE_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }
//     }
// }

// export const listUsers = () => {
//     return async (dispatch, getState) => {
//         try {
//             dispatch({ type: USER_LIST_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${getState().userLogin.userInfo.token}`
//                 }
//             }
//             const { data } = await axios.get('/api/users', config);
//             dispatch({
//                 type: USER_LIST_SUCCESS,
//                 payload: data
//             })
//         } catch (error) {
//             dispatch({
//                 type: USER_LIST_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }
//     }
// }

// export const deleteUser = (id) => {
//     return async (dispatch, getState) => {
//         try {
//             dispatch({ type: USER_DELETE_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${getState().userLogin.userInfo.token}`
//                 }
//             }
//             await axios.delete(`/api/users/${id}`, config);
//             dispatch({
//                 type: USER_DELETE_SUCCESS,
//             })
//         } catch (error) {
//             dispatch({
//                 type: USER_DELETE_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }
//     }
// }

// export const userUpdates = (user) => {
//     return async (dispatch, getState) => {
//         try {
//             dispatch({ type: USER_UPDATE_REQUEST });
//             const config = {
//                 headers: {
//                     'Content-Type': 'application/json',
//                     Authorization: `Bearer ${getState().userLogin.userInfo.token}`
//                 }
//             }
//             const { data } = await axios.put(`/api/users/${user._id}`, user, config);
//             dispatch({
//                 type: USER_UPDATE_SUCCESS,
//             })
//             dispatch({ type: USER_DETAILS_SUCCESS, payload: data });
//         } catch (error) {
//             dispatch({
//                 type: USER_UPDATE_FAIL,
//                 payload: error.response && error.response.data.message ? error.response.data.message : error.message
//             })
//         }
//     }
// }

import {
  HANDLE_AUTH_REQUEST,
  HANDLE_AUTH_SUCCESS,
  HANDLE_AUTH_FAIL,
} from "./types";
import Api from "../services/Api";
import axios from "axios";

export const handleAuth = (user) => {
  return async (dispatch) => {
    try {
      dispatch({ type: HANDLE_AUTH_REQUEST });
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      };
      const params = new URLSearchParams();
      params.append("email", "test@gmail.com");
      const data = await Api.post("/isdoctorpresent", params, config);
      // const { data } = await axios.post("/isdoctorpresent", params, config);
      localStorage.setItem(
        "user_for_route",
        JSON.stringify({
          user_for_route: data.data,
          expiry: new Date().getTime() + 1000 * 60 * 60,
        })
      );
      dispatch({
        type: HANDLE_AUTH_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      console.log("error12", error.message);
      dispatch({
        type: HANDLE_AUTH_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
};

export const doctorRegister = (doctorData) => {
  return async (dispatch) => {
    try {
      dispatch({ type: HANDLE_AUTH_REQUEST });
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      };
      const params = new URLSearchParams();
      params.append("firstname", doctorData.firstname);
      params.append("lastname", doctorData.lastname);
      params.append("cardname", doctorData.cardname);
      params.append("cardnumber", doctorData.cardnumber);
      params.append("cardcode", doctorData.cardcode);
      params.append("cardexpirydate", doctorData.cardexpirydate);
      params.append("cardzipcode", doctorData.cardzipcode);
      params.append("email", doctorData.email);
      params.append("id", "1");
      const data = await Api.post("/doctorregister", params, config);
      // const { data } = await axios.post("/doctorregister", params, config);
      localStorage.setItem(
        "user_for_route",
        JSON.stringify({
          user_for_route: data.data,
          expiry: new Date().getTime() + 1000 * 60 * 60,
        })
      );
      dispatch({
        type: HANDLE_AUTH_SUCCESS,
        payload: data.data,
      });
    } catch (error) {
      console.log("error12", error.message);
      dispatch({
        type: HANDLE_AUTH_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    }
  };
};
