import React from "react";
import { Link } from "react-router-dom";
import { Form, Row, Button, Col } from "react-bootstrap";
import EmailImg from "../assets/images/email-img.png";

const CreateAccountStepFourScreen = ({ match }) => {
  return (
    <>
      <div className="create_account_step_one create_account_step_four">
        <div className="create_account_step_four_inner_content">
          <img src={EmailImg} alt="email-img" />
          <h2 className="create_account_heding">You’re In!</h2>
          <p className="create_account_paregraph">
            We’ve sent an email to your email address to verify your email
            address.
          </p>
          <Link to={"/dashboard"} className="create_account_next_btn">
            Continue To Dashboard
          </Link>
        </div>
      </div>
    </>
  );
};

export default CreateAccountStepFourScreen;
