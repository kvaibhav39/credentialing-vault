/**
 * Wrapper function above Axios lib
 * @author Vaibhav Khunt
 */

import { BASE_URL } from "../../config";

const axios = require("axios");
/** Set base url for api */
axios.defaults.baseURL = BASE_URL;

export const setHeader = (token) => {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};
/**
 * Parse and return HTTP API response
 * @param res
 */
const getResponse = (res) => {
  console.log("response", res);
  if (res && (res.status === 200 || res.status === 201 || res.status === 204)) {
    return res.data;
  }

  throw new Error("Some error occur");
};

/**
 * Get request
 * @param path      API url path
 * @param params    Request parameters
 * @param headers    Request header
 */
export const get = (path, params) => {
  return new Promise((resolve, reject) => {
    try {
      axios
        .get(path, { params } || null)
        .then(getResponse)
        .then(resolve)
        .catch(reject);
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * Post request
 * @param path      API url path
 * @param params    Request parameters
 */
export const post = (path, params, config) => {
  return new Promise((resolve, reject) => {
    try {
      axios
        .post(path, params, config)
        .then(getResponse)
        .then(resolve)
        .catch(reject);
    } catch (error) {
      console.log("error99", error);
      reject(error);
    }
  });
};

/**
 * Delete request
 * @param path      API url path
 * @param params    Request parameters
 * @param headers   Request headers
 */
export const del = (path, params) => {
  return new Promise((resolve, reject) => {
    try {
      axios.delete(path, params).then(getResponse).then(resolve).catch(reject);
    } catch (error) {
      reject(error);
    }
  });
};

/**
 * put request
 * @param path      API url path
 * @param params    Request parameters
 * @param headers   Request headers
 */

export const put = (path, params, headers) => {
  return new Promise((resolve, reject) => {
    try {
      axios
        .put(path, params, { headers })
        .then(getResponse)
        .then(resolve)
        .catch(reject);
    } catch (error) {
      reject(error);
    }
  });
};

export default { get, post, put, del };
