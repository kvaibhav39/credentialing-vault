import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import NotFoundPage from "./screens/NotFoundPage";
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import RegisterScreen from "./screens/RegisterScreen";
import ProfileScreen from "./screens/ProfileScreen";
import DoctorAccountScreen from "./screens/DoctorAccountScreen";
import DoctorAcountPaymentScreen from "./screens/DoctorAcountPaymentScreen";
import ConfirmationScreen from "./screens/ConfirmationScreen";
import OrganizationAccountPaymentScreen from "./screens/OrganizationAccountPaymentScreen";
import OrganizationAccountScreen from "./screens/OrganizationAccountScreen";
import SelectMembershipScreen from "./screens/SelectMembershipScreen";
import DashboradScreen from "./screens/DashboradScreen";
import AccountSetupScreen from "./screens/AccountSetupScreen";
import AccountSetupDemographicScreen from "./screens/AccountSetupDemographicScreen";
import AccountSetupHomeAddressScreen from "./screens/AccountSetupHomeAddressScreen";
import AccountSetupEducationTrainingScreen from "./screens/AccountSetupEducationTrainingScreen";
import AccountSetupMedicalGraduationScreen from "./screens/AccountSetupMedicalGraduationScreen";
import AccountSetupLicensesCredentialsScreen from "./screens/AccountSetupLicensesCredentialsScreen";
import AccountSetupBoardCertificationsScreen from "./screens/AccountSetupBoardCertificationsScreen";
import AccountSetupHospitalsScreen from "./screens/AccountSetupHospitalsScreen";
import AccountSetupWorkHistoryScreen from "./screens/AccountSetupWorkHistoryScreen";
import AccountSetupGapScreen from "./screens/AccountSetupGapScreen";
import AccountSetupPeerReferencesScreen from "./screens/AccountSetupPeerReferencesScreen";
import AccountSetupMedicalHistoryScreen from "./screens/AccountSetupMedicalHistoryScreen";
import AccountSetupInsuranceScreen from "./screens/AccountSetupInsuranceScreen";
import AccountSetupClaimsHistoryScreen from "./screens/AccountSetupClaimsHistoryScreen";
import { getWithExpiry } from "./config/helper";

const App = () => {
  const [refresh, setRefresh] = useState(false);
  const user = useSelector((state) => state.user.userInfo);
  useEffect(() => {
    if (!getWithExpiry("userInfo")) {
      setTimeout(() => {
        setRefresh(!refresh);
      }, 500);
    }
  }, [getWithExpiry("userInfo"), refresh]);

  let routes;

  if (getWithExpiry("userInfo")) {
    routes = (
      <>
        <Route path="/register" component={RegisterScreen} />
        <Route path="/profile" component={ProfileScreen} />
        <Route path="/doctor-account" component={DoctorAccountScreen} />
        <Route
          path="/doctor-account-payment"
          component={DoctorAcountPaymentScreen}
        />
        <Route path="/confirmation-screen" component={ConfirmationScreen} />
        <Route
          path="/organization-account"
          component={OrganizationAccountScreen}
        />
        <Route
          path="/organization-account-payment"
          component={OrganizationAccountPaymentScreen}
        />
        <Route path="/select-membership" component={SelectMembershipScreen} />
        <Route
          path="/dashboard"
          component={DashboradScreen}
          // render={() => {
          //   if (getWithExpiry("user_for_route") === "new") {
          //     <Redirect to="/register" />;
          //   } else {
          //     // <Redirect to="/dashboard" />;
          //   }
          // }}
        />
        <Route path="/account-setup" component={AccountSetupScreen} />
        <Route
          path="/account-setup-demographic"
          component={AccountSetupDemographicScreen}
        />
        <Route
          path="/account-setup-home-address"
          component={AccountSetupHomeAddressScreen}
        />
        <Route
          path="/account-setup-education-training"
          component={AccountSetupEducationTrainingScreen}
        />
        <Route
          path="/account-setup-medical-graduation"
          component={AccountSetupMedicalGraduationScreen}
        />
        <Route
          path="/account-setup-licenses-credentials"
          component={AccountSetupLicensesCredentialsScreen}
        />
        <Route
          path="/account-setup-board-certifications"
          component={AccountSetupBoardCertificationsScreen}
        />
        <Route
          path="/account-setup-hospitals"
          component={AccountSetupHospitalsScreen}
        />
        <Route
          path="/account-setup-work-history"
          component={AccountSetupWorkHistoryScreen}
        />
        <Route path="/account-setup-gap" component={AccountSetupGapScreen} />
        <Route
          path="/account-setup-peer-references"
          component={AccountSetupPeerReferencesScreen}
        />
        <Route
          path="/account-setup-medical-history"
          component={AccountSetupMedicalHistoryScreen}
        />
        <Route
          path="/account-setup-insurance"
          component={AccountSetupInsuranceScreen}
        />
        <Route
          path="/account-setup-claims-history"
          component={AccountSetupClaimsHistoryScreen}
        />
      </>
    );
  }

  return (
    <Router>
      {getWithExpiry("userInfo") && <Header />}
      <Switch>
        <Route path="/" component={HomeScreen} exact />
        {routes}
        <Route component={NotFoundPage} />
      </Switch>
      <Footer />
    </Router>
  );
};

export default App;
