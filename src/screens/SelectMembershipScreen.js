import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Row, Button, Col } from 'react-bootstrap';

const SelectMembershipScreen = ({ match }) => {

    return (
        <>
            <div className="create_account_step_one select_membership_section">
                <h2 className="create_account_heding">Select Membership</h2>
                <p className="create_account_paregraph">
                    Enter your billing information below to finish creating your Credentialing Vault account.
                </p>
                <div className="membership_price_content">
                    <div className="membership_price_user_text">
                        <p>Price:</p>
                        <p>Users:</p>
                    </div>
                    <Row className="membership_price_row">
                        <Link to={'/select-membership'} className="membership_price_column">
                            <div className="membership_price_month_text active">
                                <div className="membership_price_texts">
                                    <h2>Basic</h2>
                                    <h4>$75/month</h4>
                                </div>
                                <div className="membership_month_text">
                                    <p>1</p>
                                </div>
                            </div>
                        </Link>
                        <Link to={'/select-membership'} className="membership_price_column">
                            <div className="membership_price_month_text">
                                <div className="membership_price_texts">
                                    <h2>Standard</h2>
                                    <h4>$100/month</h4>
                                </div>
                                <div className="membership_month_text">
                                    <p>1-5</p>
                                </div>
                            </div>
                        </Link>
                        <Link to={'/select-membership'} className="membership_price_column">
                            <div className="membership_price_month_text">
                                <div className="membership_price_texts">
                                    <h2>Super</h2>
                                    <h4>$150/month</h4>
                                </div>
                                <div className="membership_month_text">
                                    <p>6-25</p>
                                </div>
                            </div>
                        </Link>
                        <Link to={'/select-membership'} className="membership_price_column">
                            <div className="membership_price_month_text">
                                <div className="membership_price_texts">
                                    <h2>Pro</h2>
                                    <h4>Contact Us</h4>
                                </div>
                                <div className="membership_month_text">
                                    <p>26-100</p>
                                </div>
                            </div>
                        </Link>
                        <Link to={'/select-membership'} className="membership_price_column">
                            <div className="membership_price_month_text">
                                <div className="membership_price_texts">
                                    <h2>Pro+</h2>
                                    <h4>Contact Us</h4>
                                </div>
                                <div className="membership_month_text">
                                    <p>Unlimited</p>
                                </div>
                            </div>
                        </Link>
                    </Row>
                </div>
                <Link to={'/dashborad'} className="create_account_next_btn">Next</Link>
                <Link to={'/register'} className="create_account_back_btn">Back</Link>
            </div>
        </>
    )
}

export default SelectMembershipScreen;
