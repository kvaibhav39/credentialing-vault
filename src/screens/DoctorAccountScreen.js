import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Form, Row, Col } from "react-bootstrap";
import { getWithExpiry } from "../config/helper";

const CreateAccountStepTwoScreen = ({ match }) => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState(
    getWithExpiry("userInfo")?.userInfo?.email
  );
  console.log("emaill", email);

  return (
    <>
      <div className="create_account_step_one create_account_step_two">
        <h2 className="create_account_heding">Create Your Doctor Account</h2>
        <p className="create_account_paregraph">
          Your doctor account will allow you to credential your information with
          other organization.
        </p>
        <Row className="creat_account_input_row">
          <Col md={6}>
            <Form.Group>
              <Form.Label>First Name:</Form.Label>
              <Form.Control
                type="text"
                value={firstname}
                onChange={(e) => setFirstname(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
              <Form.Label>Last Name:</Form.Label>
              <Form.Control
                type="text"
                value={lastname}
                onChange={(e) => setLastname(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={12}>
            <Form.Group>
              <Form.Label>Email Address:</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
          </Col>
          {/* <Col md={6}>
            <Form.Group>
              <Form.Label>Password:</Form.Label>
              <Form.Control type="password" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
              <Form.Label>Confirm Password:</Form.Label>
              <Form.Control type="password" />
            </Form.Group>
          </Col> */}
        </Row>
        <Link
          className={`create_account_next_btn ${
            (firstname.length === 0 ||
              lastname.length === 0 ||
              !new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(
                email
              )) &&
            "disabled"
          }`}
          to={{
            pathname: "/doctor-account-payment",
            state: {
              firstname,
              lastname,
              email,
            },
          }}
        >
          Next
        </Link>
        <Link to={"/register"} className="create_account_back_btn">
          Back
        </Link>
      </div>
    </>
  );
};

export default CreateAccountStepTwoScreen;
