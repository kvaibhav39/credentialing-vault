export const USER_LOGIN_REQUEST = "user_login_request";
export const USER_LOGIN_SUCCESS = "user_login_success";
export const USER_LOGIN_FAIL = "user_login_fail";
export const USER_LOGOUT = "user_logout";

export const USER_REGISTER_REQUEST = "user_register_request";
export const USER_REGISTER_SUCCESS = "user_register_success";
export const USER_REGISTER_FAIL = "user_register_fail";

export const USER_LIST_REQUEST = "user_list_request";
export const USER_LIST_SUCCESS = "user_list_success";
export const USER_LIST_FAIL = "user_list_fail";
export const USER_LIST_RESET = "user_list_reset";

export const USER_DETAILS_REQUEST = "user_details_request";
export const USER_DETAILS_SUCCESS = "user_details_success";
export const USER_DETAILS_FAIL = "user_details_fail";
export const USER_DETAILS_RESET = "user_details_reset";

export const USER_UPDATE_PROFILE_REQUEST = "user_update_profile_request";
export const USER_UPDATE_PROFILE_SUCCESS = "user_update_profile_success";
export const USER_UPDATE_PROFILE_FAIL = "user_update_profile_fail";
export const USER_UPDATE_PROFILE_RESET = "user_update_profile_reset";

export const USER_DELETE_REQUEST = "user_delete_request";
export const USER_DELETE_SUCCESS = "user_delete_success";
export const USER_DELETE_FAIL = "user_delete_fail";

export const USER_UPDATE_REQUEST = "user_update_request";
export const USER_UPDATE_SUCCESS = "user_update_success";
export const USER_UPDATE_FAIL = "user_update_fail";
export const USER_UPDATE_RESET = "user_update_reset";

export const HANDLE_AUTH_REQUEST = "HANDLE_AUTH_REQUEST";
export const HANDLE_AUTH_SUCCESS = "HANDLE_AUTH_SUCCESS";
export const HANDLE_AUTH_FAIL = "HANDLE_AUTH_FAIL";
