import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { handleAuth } from "../redux/actions/userActions";
import { getWithExpiry } from "../config/helper";

const HandleRegister = () => {
  const [state, setState] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state) => state.user.userInfo);
  useEffect(() => {
    if (!getWithExpiry("userInfo")) {
      setTimeout(() => {
        setState(!state);
      }, 1000);
    }
    if (getWithExpiry("userInfo")) {
      const tempUser = getWithExpiry("userInfo");
      dispatch(handleAuth(tempUser.userInfo));
    }
  }, [getWithExpiry("userInfo"), state]);
  useEffect(() => {
    if (!user) {
      history.push("/dashboard");
    } else if (user && user !== 1) {
      history.push("/register");
    }
  }, [user]);
  return null;
};

export default HandleRegister;
