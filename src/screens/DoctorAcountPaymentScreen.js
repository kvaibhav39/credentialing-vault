import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Form, Row, Col } from "react-bootstrap";
import { doctorRegister } from "../redux/actions/userActions";

const CreateAccountStepThreeScreen = ({ location }) => {
  const [cardname, setCardName] = useState("");
  const [cardnumber, setCardnumber] = useState("");
  const [cardcode, setCardcode] = useState("");
  const [cardexpirydate, setCardexpirydate] = useState("");
  const [cardzipcode, setCardzipcode] = useState("");
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  const user = useSelector((state) => state.user.userInfo);

  useEffect(() => {
    if (user && user !== 1 && user !== "new") {
      history.push("/confirmation-screen");
    } else {
      setError("You are already register");
    }
  }, [user]);
  const data = {
    ...location.state,
    cardname,
    cardnumber,
    cardcode,
    cardexpirydate,
    cardzipcode,
  };
  console.log("datatat", data);
  const doctorRegisterHandler = () => {
    const { firstname, lastname, email } = location.state;
    if (
      cardname.length > 0 &&
      cardnumber.length > 0 &&
      cardcode.length > 0 &&
      cardexpirydate.length > 0 &&
      cardzipcode.length > 0 &&
      firstname.length > 0 &&
      lastname.length > 0 &&
      email.length > 0
    ) {
      const data = {
        ...location.state,
        cardname,
        cardnumber,
        cardcode,
        cardexpirydate,
        cardzipcode,
      };
      dispatch(doctorRegister(data));
    } else {
      setError("Invalid Data");
    }
  };

  return (
    <>
      <div className="create_account_step_one create_account_step_three">
        <h2 className="create_account_heding">Create Account</h2>
        <p className="create_account_paregraph">
          Enter your billing information below to finish creating your
          Credentialing Vault account.
        </p>
        <div className="create_account_price_content">
          <h3>$50/month</h3>
          <p>Cancel Anytime</p>
        </div>
        <Row className="creat_account_input_row">
          <Col md={12}>
            <Form.Group>
              <Form.Label>Cardholder’s Name:</Form.Label>
              <Form.Control
                type="text"
                value={cardname}
                onChange={(e) => setCardName(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={8}>
            <Form.Group>
              <Form.Label>Card Number:</Form.Label>
              <Form.Control
                type="text"
                placeholder="0000 0000 0000 0000"
                value={cardnumber}
                onChange={(e) => setCardnumber(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group>
              <Form.Label>Security Code:</Form.Label>
              <Form.Control
                type="text"
                placeholder="CVC"
                value={cardcode}
                onChange={(e) => setCardcode(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
              <Form.Label>Expiration Date:</Form.Label>
              <Form.Control
                type="date"
                placeholder="MM/YYYY"
                value={cardexpirydate}
                onChange={(e) => setCardexpirydate(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group>
              <Form.Label>Zip Code:</Form.Label>
              <Form.Control
                type="text"
                placeholder="12345"
                value={cardzipcode}
                onChange={(e) => setCardzipcode(e.target.value)}
              />
            </Form.Group>
          </Col>
        </Row>
        {error.length > 0 && <h1>{error}</h1>}
        <a className="create_account_next_btn" onClick={doctorRegisterHandler}>
          Pay Now - $50.00
        </a>
        <Link to={"/doctor-account"} className="create_account_back_btn">
          Back
        </Link>
      </div>
    </>
  );
};

export default CreateAccountStepThreeScreen;
