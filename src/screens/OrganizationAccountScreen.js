import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Row, Button, Col } from 'react-bootstrap';

const CreateAccountStepTwoOrgScreen = ({ match }) => {

    return (
        <>
            <div className="create_account_step_one create_account_step_two">
                <h2 className="create_account_heding">Create Your Organization Account</h2>
                <p className="create_account_paregraph">Your organization account will allow you to view credentialed information.</p>
                <Row className="creat_account_input_row">
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>First Name:</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>Last Name:</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                    </Col>
                    <Col md={12}>
                        <Form.Group>
                            <Form.Label>Company:</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                    </Col>
                    <Col md={12}>
                        <Form.Group>
                            <Form.Label>Email Address:</Form.Label>
                            <Form.Control type="email" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>Password:</Form.Label>
                            <Form.Control type="password" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>Confirm Password:</Form.Label>
                            <Form.Control type="password" />
                        </Form.Group>
                    </Col>
                </Row>
                <Link to={'/select-membership'} className="create_account_next_btn">Next</Link>
                <Link to={'/register'} className="create_account_back_btn">Back</Link>
            </div>
        </>
    )
}

export default CreateAccountStepTwoOrgScreen;
