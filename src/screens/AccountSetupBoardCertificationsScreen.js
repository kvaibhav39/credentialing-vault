import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";

const AccountSetupBoardCertificationsScreen = ({ match }) => {
  const [certified, setCertified] = useState("");
  console.log("certified", certified);
  const [inputList, setInputList] = useState([
    {
      // user_email: getWithExpiry("userInfo").userInfo.email,
      boardname: "",
      certificatenumber: "",
      certificatestatus: "",
      speciality: "",
      initialcertification: "",
      recertification: "",
      certificationexpires: "",
      schedualexamdate: "",
      certificate: "",
      vdate: "",
      document: "",
    },
  ]);

  console.log("inputList", inputList);
  const handleInputChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      {
        // user_email: getWithExpiry("userInfo").userInfo.email,
        boardname: "",
        certificatenumber: "",
        certificatestatus: "",
        speciality: "",
        initialcertification: "",
        recertification: "",
        certificationexpires: "",
        schedualexamdate: "",
        certificate: "",
        vdate: "",
        document: "",
      },
    ]);
  };

  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Board Certifcations"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check type="checkbox" label="Hospitals" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check type="checkbox" label="Work History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Board Certifications" />
              <p>Are you board certified?</p>
              <div className="heading_radio_btn custom_radio_btn_content">
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="yes"
                    type="radio"
                    name="certified"
                    value="yes"
                    onChange={(e) => setCertified(e.target.value)}
                    checked={certified === "yes"}
                  />
                  <label for="yes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="no"
                    type="radio"
                    name="certified"
                    value="no"
                    onChange={(e) => setCertified(e.target.value)}
                    checked={certified === "no"}
                  />
                  <label for="no">No</label>
                </div>
              </div>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">
                List all board certifications or previous held. If you are
                eligible or in-process, please indicate in the ‘Certification
                Status’.
              </p>
              {inputList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Board Name:</Form.Label>
                        <Form.Control
                          type="text"
                          name="boardname"
                          value={x.boardname}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Certificate Number:</Form.Label>
                        <Form.Control
                          type="text"
                          name="certificatenumber"
                          value={x.certificatenumber}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Certificate Status:</Form.Label>
                        <Form.Control
                          type="text"
                          name="certificatestatus"
                          value={x.certificatestatus}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Speciality:</Form.Label>
                        <Form.Control
                          type="text"
                          name="speciality"
                          value={x.speciality}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Initial Certification:</Form.Label>
                        <Form.Control
                          type="text"
                          name="initialcertification"
                          value={x.initialcertification}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Recertification:</Form.Label>
                        <Form.Control
                          type="text"
                          name="recertification"
                          value={x.recertification}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Certification Expires:</Form.Label>
                        <Form.Control
                          type="text"
                          name="certificationexpires"
                          value={x.certificationexpires}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Scheduled Exam Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="schedualexamdate"
                          value={x.schedualexamdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Certificate:</Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="certificate"
                            value={x.certificate}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="vdate"
                          value={x.vdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Documentation:</Form.Label>
                        <Form.Control
                          type="text"
                          name="document"
                          value={x.document}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button className="pluse_add_text_btn" onClick={handleAddClick}>
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
                Add
              </Button>
              <div className="next_back_btns">
                <Link
                  to={"/account-setup-licenses-credentials"}
                  className="back_btn"
                >
                  Back
                </Link>
                <Link to={"/account-setup-hospitals"} className="next_btn">
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupBoardCertificationsScreen;
