import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Row, Button, Col } from 'react-bootstrap';

const CreateAccountStepThreeScreen = ({ match }) => {

    return (
        <>
            <div className="create_account_step_one create_account_step_three">
                <h2 className="create_account_heding">Create Account</h2>
                <p className="create_account_paregraph">Enter your billing information below to finish creating your Credentialing Vault account.</p>
                <div className="create_account_price_content">
                    <p>Basic Membership</p>
                    <h3>$75/month</h3>
                    <p>Cancel Anytime</p>
                </div>
                <Row className="creat_account_input_row">
                    <Col md={12}>
                        <Form.Group>
                            <Form.Label>Cardholder’s Name:</Form.Label>
                            <Form.Control type="text" />
                        </Form.Group>
                    </Col>
                    <Col md={8}>
                        <Form.Group>
                            <Form.Label>Card Number:</Form.Label>
                            <Form.Control type="number" placeholder="0000 0000 0000 0000" />
                        </Form.Group>
                    </Col>
                    <Col md={4}>
                        <Form.Group>
                            <Form.Label>Security Code:</Form.Label>
                            <Form.Control type="number" placeholder="CVC" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>Expiration Date:</Form.Label>
                            <Form.Control type="text" placeholder="MM/YYYY" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group>
                            <Form.Label>Zip Code:</Form.Label>
                            <Form.Control type="number" placeholder="12345" />
                        </Form.Group>
                    </Col>
                </Row>
                <Link to={'/register'} className="create_account_next_btn">Pay Now - $75.00</Link>
                <Link to={'/create-account-step-two-org'} className="create_account_back_btn">Back</Link>
            </div>
        </>
    )
}

export default CreateAccountStepThreeScreen;
