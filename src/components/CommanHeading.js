import React from 'react'

const CommanHeading = (props) => {
    return (
        <>
            <div className="comman_heading_section">
                <h2 className="comman_main_heading">{props.heading}</h2>
                <h3 className="comman_sub_heading">{props.subHeading}</h3>
                <p className="comman_paragraph">{props.paragraph}</p>
            </div>
        </>
    )
}

export default CommanHeading
