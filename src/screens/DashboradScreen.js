import React from "react";
import { useHistory } from "react-router-dom";
import LeftSidebar from "../components/LeftSidebar";
import CommanHeading from "../components/CommanHeading";
import authClient from "../sdk/api";
import { getWithExpiry } from "../config/helper";

const DashboradScreen = ({ match }) => {
  const history = useHistory();

  const clearSession = () => {
    localStorage.removeItem("userInfo");
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("state");
    localStorage.removeItem("nonce");
    localStorage.removeItem("id_token_json");
  };

  const handleSignOff = () => {
    if (getWithExpiry("id_token")) {
      authClient.signOff(
        getWithExpiry("id_token")?.id_token,
        getWithExpiry("state").state
      );
    } else {
      history.push("/");
    }
    clearSession();
  };

  return (
    <>
      <div className="input-field">
        <button type="button" onClick={handleSignOff}>
          {" "}
          Sign Off
        </button>
      </div>
      <main className="dashoard_page_screen">
        <section className="comman_left_right_side_row dashoard_section">
          <LeftSidebar />
          <div className="comman_right_content">
            <label
              for="leftSidebarToggelInput"
              className="left_sidebar_toggel_btn"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
            </label>
            <CommanHeading heading="Dashboard" subHeading="Getting started" />
            <div className="dashboard_step_section">
              <div className="dashboard_step_item">
                <div className="dashboard_step_img">
                  <img
                    src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
                    alt="step pic"
                  />
                </div>
                <div className="dashboard_step_texts">
                  <h3>Step 1</h3>
                  <h2>Set Up 2 Factor Authentication</h2>
                  <p>Keep your account secure by entering your phone number.</p>
                </div>
                <div className="dashboard_step_continue_btn">
                  <button type="button">Continue</button>
                </div>
              </div>
              <div className="dashboard_step_item">
                <div className="dashboard_step_img">
                  <img
                    src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
                    alt="step pic"
                  />
                </div>
                <div className="dashboard_step_texts">
                  <h3>Step 2</h3>
                  <h2>Create Profile</h2>
                  <p>
                    Let’s get you started by completing your Credentialing Vault
                    profile.{" "}
                  </p>
                </div>
                <div className="dashboard_step_continue_btn">
                  <button
                    type="button"
                    onClick={() => {
                      history.push("/account-setup");
                    }}
                  >
                    Continue
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
};

export default DashboradScreen;
