import React from "react";
import { Link } from "react-router-dom";

const AccountSetupScreen = ({ match }) => {
  return (
    <>
      <div className="account_setup_page_section">
        <div className="container">
          <div className="account_setup_page_content">
            <img
              src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
              alt="user img"
              className="account_setup_img"
            />
            <h2>Instructions</h2>
            <p>
              We’re going to get started building out your Credentialing Vault
              profile. You may save and exit at any time.
            </p>
            <Link
              to={"/account-setup-demographic"}
              className="account_setup_get_start_btn"
            >
              Get Started
            </Link>
            <Link to={"/dashboard"} className="account_setup_exit_btn">
              Exit
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default AccountSetupScreen;
