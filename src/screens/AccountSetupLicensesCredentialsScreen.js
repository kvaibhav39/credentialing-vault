import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";
import { getWithExpiry } from "../config/helper";

const AccountSetupMedicalGraduationScreen = ({ match }) => {
  const [multipledata, setMultipledata] = useState([
    {
      type: "",
      number: "",
      initiatedate: "",
      expiredate: "",
      comment: "",
      login: "",
      password: "",
      vdate: "",
      documents: "",
      profile: "",
    },
  ]);
  const [pid, setPid] = useState("");
  const [dateissue, setDateissue] = useState("");
  const [dateexpire, setDateexpire] = useState("");
  const [slogin, setSlogin] = useState("");
  const [spassword, setSpassword] = useState("");
  const [medicareid, setMedicareid] = useState("");
  const [mdateissue, setMdateissue] = useState("");
  const [mdateexpire, setMdateexpire] = useState("");
  const [mslogin, setMslogin] = useState("");
  const [mspassword, setMspassword] = useState("");
  const [medicarepid, setMedicarepid] = useState("");
  const [medicaredateissue, setMedicaredateissue] = useState("");
  const [medicaredateexpire, setMedicaredateexpire] = useState("");
  const [medicarsplogin, setMedicarsplogin] = useState("");
  const [medicaresppassword, setMedicaresppassword] = useState("");

  console.log("multipledata", multipledata);
  const handleInputChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...multipledata];
    list[index][name] = value;
    setMultipledata(list);
  };

  const handleAddClick = () => {
    setMultipledata([
      ...multipledata,
      {
        type: "",
        number: "",
        initiatedate: "",
        expiredate: "",
        comment: "",
        login: "",
        password: "",
        vdate: "",
        documents: "",
        profile: "",
      },
    ]);
  };

  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check type="checkbox" label="Board Certifcations" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check type="checkbox" label="Hospitals" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check type="checkbox" label="Work History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Licenses / Credentials " />
              <p>
                List all state licenses ever held (active and inactive), DEA (if
                applicable), and other certificates (ACLS, BLS, PALS). Please
                document any limitations on your licenses in the comments box.
              </p>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">
                License or Credential:
              </p>
              {multipledata.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Type:</Form.Label>
                        <Form.Control
                          type="text"
                          name="type"
                          value={x.type}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>License Number:</Form.Label>
                        <Form.Control
                          type="text"
                          name="number"
                          value={x.number}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Initial Issue Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="initiatedate"
                          value={x.initiatedate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Expiration Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="expiredate"
                          value={x.expiredate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Comments:</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="comment"
                          value={x.comment}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>State Login:</Form.Label>
                        <Form.Control
                          type="text"
                          name="login"
                          value={x.login}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Password:</Form.Label>
                        <Form.Control
                          type="password"
                          name="password"
                          value={x.password}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>License:</Form.Label>
                        <Button className="input_file_button">
                          <Form.Control
                            type="file"
                            name="profile"
                            value={x.profile}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                          Upload
                        </Button>
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Date:</Form.Label>
                        <Form.Control
                          type="text"
                          name="vdate"
                          value={x.vdate}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Verified Documentation:</Form.Label>
                        <Form.Control
                          type="text"
                          name="documents"
                          value={x.documents}
                          onChange={(e) => handleInputChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button className="pluse_add_text_btn" onClick={handleAddClick}>
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
                Add
              </Button>
              <div className="preferred_contact_radio_btn custom_radio_btn_content">
                <p className="preferred_contact_radio_peragraph_heading">
                  Do you have a state prescription account?
                </p>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="prescriptionAccountYes"
                    type="radio"
                    name="prescriptionAccount"
                  />
                  <label for="prescriptionAccountYes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="prescriptionAccountNo"
                    type="radio"
                    name="prescriptionAccount"
                  />
                  <label for="prescriptionAccountNo">No</label>
                </div>
              </div>
              <div className="preferred_contact_radio_btn custom_radio_btn_content">
                <p className="preferred_contact_radio_peragraph_heading">
                  Do you have a state <span>Medicare</span> provider?
                </p>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="medicareYes" type="radio" name="medicare" />
                  <label for="medicareYes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="medicareNo" type="radio" name="medicare" />
                  <label for="medicareNo">No</label>
                </div>
              </div>
              <div className="preferred_contact_radio_btn custom_radio_btn_content">
                <p className="preferred_contact_radio_peragraph_heading">
                  Do you have a state <span>Medicaid</span> provider?
                </p>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="medicaidYes" type="radio" name="medicaid" />
                  <label for="medicaidYes">Yes</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input id="medicaidNo" type="radio" name="medicaid" />
                  <label for="medicaidNo">No</label>
                </div>
              </div>
              <div className="next_back_btns">
                <Link
                  to={"/account-setup-medical-graduation"}
                  className="back_btn"
                >
                  Back
                </Link>
                <Link
                  to={"/account-setup-board-certifications"}
                  className="next_btn"
                >
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupMedicalGraduationScreen;
