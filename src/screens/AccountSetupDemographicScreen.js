import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";

const AccountSetupDemographicScreen = ({ match }) => {
  const [suffix, setSuffix] = useState("Mr.");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [middlename, setMiddlename] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [birthplace, setBirthplace] = useState("");
  const [ssn, setSsn] = useState("");
  const [npi, setNpi] = useState("");
  const [upin, setUpin] = useState("");
  const [citizenship, setCitizenship] = useState();
  const [pobc, setPobc] = useState("");
  const [spousename, setSpousename] = useState("");
  const [maritalstatus, setMaritalstatus] = useState("");
  const [inputList, setInputList] = useState([
    { othername: "", startdate: "", enddate: "", certificate: "" },
  ]);
  const [language, setLanguage] = useState([{ lg: "" }]);
  // const [lang, setLang] = useState(["English", "Hindi", "Gujarati"]);
  const [email, setEmail] = useState("");
  const [cellphone, setCellphone] = useState("");
  const [pager, setPager] = useState("");
  const [pcm, setPcm] = useState("");
  // console.log("language", language, lang, suffix);

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  const handleAddClick = () => {
    setInputList([
      ...inputList,
      { othername: "", startdate: "", enddate: "", certificate: "" },
    ]);
  };

  const handleLanguage = (e, index) => {
    const { name, value } = e.target;
    const list = [...language];
    list[index][name] = value !== "Select" ? value : "English";
    // let tempIndex = lang.indexOf(value);
    // let templang = [...lang];
    // templang.splice(tempIndex, 1);
    // setLang(templang);
    setLanguage(list);
  };

  const handleAddLanuage = () => {
    setLanguage([...language, { lg: "" }]);
  };

  return (
    <>
      <div className="account_setup_content">
        <input
          type="checkbox"
          id="accountSetupToggel"
          className="accountSetupToggleInput"
        />
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <label
              for="accountSetupToggel"
              className="accountSetupToggleCloseBtn"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </label>
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check type="checkbox" label="Home Address" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check type="checkbox" label="Education / Training" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check type="checkbox" label="Board Certifcations" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check type="checkbox" label="Hospitals" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check type="checkbox" label="Work History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section">
          <div className="account_setup_right_content">
            <label for="accountSetupToggel" className="accountSetupToggelBtn">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h7"
                />
              </svg>
            </label>
            <CommanHeading
              heading="Demographic"
              paragraph="Complete all applicable information requested."
            />
            <div className="acount_setup_input_content">
              <span className="indicate_required_text">
                * Indicated required
              </span>
              <h4 className="acount_setup_input_main_heading">
                Personal Details:
              </h4>
              <Row className="acount_setup_input_label_row">
                <Col lg={12}>
                  <Form.Group
                    className="acount_setup_input_label suffix_select_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <label>Suffix:</label>
                    <select
                      onChange={(e) => {
                        setSuffix(e.target.value);
                      }}
                      value={suffix}
                    >
                      <option>Mr.</option>
                      <option>Mrs.</option>
                    </select>
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>First Name:</Form.Label>
                    <Form.Control
                      type="text"
                      value={firstname}
                      onChange={(e) => setFirstname(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control
                      type="text"
                      value={lastname}
                      onChange={(e) => setLastname(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Middle Name:</Form.Label>
                    <Form.Control
                      type="text"
                      value={middlename}
                      onChange={(e) => setMiddlename(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Birth Date:</Form.Label>
                    <Form.Control
                      type="date"
                      value={birthdate}
                      onChange={(e) => setBirthdate(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={8}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Birth Place:</Form.Label>
                    <Form.Control
                      type="text"
                      value={birthplace}
                      onChange={(e) => setBirthplace(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>SSN:</Form.Label>
                    <Form.Control
                      type="text"
                      value={ssn}
                      onChange={(e) => setSsn(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>NPI:</Form.Label>
                    <Form.Control
                      type="text"
                      value={npi}
                      onChange={(e) => setNpi(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>UPIN:</Form.Label>
                    <Form.Control
                      type="text"
                      value={upin}
                      onChange={(e) => setUpin(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={4}>
                  <Form.Group
                    className="acount_setup_input_label suffix_select_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <label>Citizenship:</label>
                    <select
                      value={citizenship}
                      onChange={(e) => setCitizenship(e.target.value)}
                    >
                      <option>Other</option>
                    </select>
                  </Form.Group>
                </Col>
                <Col lg={12}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Passport or Birth Cirtificate:</Form.Label>
                    <Button className="input_file_button">
                      <Form.Control
                        type="file"
                        value={pobc}
                        onChange={(e) => setPobc(e.target.value)}
                      />
                      Upload
                    </Button>
                  </Form.Group>
                  <Row className="acount_setup_input_label_row">
                    <Col lg={8}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Spouse Name:</Form.Label>
                        <Form.Control
                          type="text"
                          value={spousename}
                          onChange={(e) => setSpousename(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={4}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>Marital Status:</label>
                        <select
                          onChange={(e) => {
                            setMaritalstatus(e.target.value);
                          }}
                          value={maritalstatus}
                        >
                          <option>Married</option>
                          <option>Unmaried</option>
                        </select>
                      </Form.Group>
                    </Col>
                  </Row>

                  {/* <Button className="pluse_add_btn">
                    <svg
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                    </svg>
                  </Button> */}
                </Col>
              </Row>
              <Row className="acount_setup_input_label_row">
                {inputList.map((x, i) => {
                  return (
                    <>
                      <Col lg={6}>
                        <Form.Group
                          className="acount_setup_input_label"
                          controlId="exampleForm.ControlInput1"
                        >
                          <Form.Label>Other Names Used:</Form.Label>
                          <Form.Control
                            type="text"
                            name="othername"
                            value={x.othername}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                        </Form.Group>
                      </Col>
                      <Col lg={3}>
                        <Form.Group
                          className="acount_setup_input_label"
                          controlId="exampleForm.ControlInput1"
                        >
                          <Form.Label>Start Date:</Form.Label>
                          <Form.Control
                            type="date"
                            name="startdate"
                            value={x.startdate}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                        </Form.Group>
                      </Col>
                      <Col lg={3}>
                        <Form.Group
                          className="acount_setup_input_label"
                          controlId="exampleForm.ControlInput1"
                        >
                          <Form.Label>End Date:</Form.Label>
                          <Form.Control
                            type="date"
                            name="enddate"
                            value={x.enddate}
                            onChange={(e) => handleInputChange(e, i)}
                          />
                        </Form.Group>
                      </Col>
                      <Col lg={12}>
                        <Form.Group
                          className="acount_setup_input_label"
                          controlId="exampleForm.ControlInput1"
                        >
                          <Form.Label>Cirtificate:</Form.Label>
                          <Button className="input_file_button">
                            <Form.Control
                              type="file"
                              name="certificate"
                              value={x.certificate}
                              onChange={(e) => handleInputChange(e, i)}
                            />
                            Upload
                          </Button>
                        </Form.Group>
                      </Col>
                    </>
                  );
                })}
                <Button className="pluse_add_btn" onClick={handleAddClick}>
                  <svg
                    width="14"
                    height="14"
                    viewBox="0 0 14 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                  </svg>
                </Button>
              </Row>
              <Row className="acount_setup_input_label_row">
                {language.map((x, i) => {
                  console.log("xcc", x);
                  return (
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <label>Language:</label>
                        <select
                          value={x.lg}
                          name="lg"
                          onChange={(e) => handleLanguage(e, i)}
                        >
                          <option>Select</option>
                          <option>English</option>
                          <option>Hindi</option>
                          <option>Gujarati</option>
                          {/* {lang.map((l) => {
                            return <option>{l}</option>;
                          })} */}
                        </select>
                      </Form.Group>
                    </Col>
                  );
                })}
                {language.length < 3 && (
                  <Button className="pluse_add_btn" onClick={handleAddLanuage}>
                    <svg
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                    </svg>
                  </Button>
                )}
              </Row>
              <Row className="acount_setup_input_label_row">
                <h4 className="acount_setup_input_main_heading">
                  Contact Information:
                </h4>
                <Col lg={12}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Email:</Form.Label>
                    <Form.Control
                      type="email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={6}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Cell Phone:</Form.Label>
                    <Form.Control
                      type="text"
                      value={cellphone}
                      onChange={(e) => setCellphone(e.target.value)}
                    />
                  </Form.Group>
                </Col>
                <Col lg={6}>
                  <Form.Group
                    className="acount_setup_input_label"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label>Pager:</Form.Label>
                    <Form.Control
                      type="text"
                      value={pager}
                      onChange={(e) => setPager(e.target.value)}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <div className="preferred_contact_radio_btn custom_radio_btn_content">
                <h4 className="acount_setup_input_main_heading">
                  Preferred Contact Method:
                </h4>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="email"
                    type="radio"
                    name="preferredContact"
                    value="email"
                    checked={pcm === "email"}
                    onChange={(e) => setPcm(e.target.value)}
                  />
                  <label for="email">Email</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="phone"
                    type="radio"
                    name="preferredContact"
                    value="phone"
                    checked={pcm === "phone"}
                    onChange={(e) => setPcm(e.target.value)}
                  />
                  <label for="phone">Phone</label>
                </div>
                <div className="preferred_contact_radio_input custom_radio_btn">
                  <input
                    id="pager"
                    type="radio"
                    name="preferredContact"
                    value="pager"
                    checked={pcm === "pager"}
                    onChange={(e) => setPcm(e.target.value)}
                  />
                  <label for="pager">Pager</label>
                </div>
              </div>
              <div className="next_back_btns">
                <Link to={"/account-setup"} className="back_btn">
                  Back
                </Link>
                <Link to={"/account-setup-home-address"} className="next_btn">
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupDemographicScreen;
