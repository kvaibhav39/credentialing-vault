import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Row, Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import CommanHeading from "../components/CommanHeading";

const AccountSetupGapScreen = ({ match }) => {
  const [workHistoryList, setWorkHistoryList] = useState([
    {
      // user_email: getWithExpiry("userInfo").userInfo.email,
      gapStartDate: "",
      gapEndDate: "",
      explanation: "",
    },
  ]);
  const [insuranceHistoryList, setInsuranceHistoryList] = useState([
    {
      // user_email: getWithExpiry("userInfo").userInfo.email,
      gapStartDate: "",
      gapEndDate: "",
      explanation: "",
    },
  ]);

  console.log("workHistoryList", workHistoryList);
  const handleWorkHistoryListChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...workHistoryList];
    list[index][name] = value;
    setWorkHistoryList(list);
  };

  const handleWorkHistoryListAddClick = () => {
    setWorkHistoryList([
      ...workHistoryList,
      {
        // user_email: getWithExpiry("userInfo").userInfo.email,
        gapStartDate: "",
        gapEndDate: "",
        explanation: "",
      },
    ]);
  };

  console.log("insuranceHistoryList", insuranceHistoryList);
  const handleInsuranceHistoryListChange = (e, index) => {
    console.log("e.traget.value", e.target.value, index);
    const { name, value } = e.target;
    const list = [...insuranceHistoryList];
    list[index][name] = value;
    setInsuranceHistoryList(list);
  };

  const handleInsuranceHistoryListAddClick = () => {
    setInsuranceHistoryList([
      ...insuranceHistoryList,
      {
        // user_email: getWithExpiry("userInfo").userInfo.email,
        gapStartDate: "",
        gapEndDate: "",
        explanation: "",
      },
    ]);
  };
  return (
    <>
      <div className="account_setup_content">
        <section className="account_setup_left_sidebar_section">
          <div className="account_setup_left_sidebar_content">
            <div className="account_setup_save_exit_btn">
              <Link to="/account-setup">Save & Exit</Link>
            </div>
            <div className="account_setup_menu">
              <ul className="account_setup_nav">
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox1"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Demographic"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox2"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Home Address"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox3"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Education / Training"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox4"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Foreign Medical Graduation"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox5"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Licenses / Credentials"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox6"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Board Certifcations"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox7"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Hospitals"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox8"
                  >
                    <Form.Check
                      type="checkbox"
                      label="Work History"
                      defaultChecked
                    />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox9"
                  >
                    <Form.Check type="checkbox" label="Gap" defaultChecked />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox10"
                  >
                    <Form.Check type="checkbox" label="Peer References" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox11"
                  >
                    <Form.Check type="checkbox" label="Medical History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox12"
                  >
                    <Form.Check type="checkbox" label="Insurance" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox13"
                  >
                    <Form.Check type="checkbox" label="Claims History" />
                  </Form.Group>
                </li>
                <li className="account_setup_items">
                  <Form.Group
                    className="account_setup_link"
                    controlId="checkbox14"
                  >
                    <Form.Check type="checkbox" label="Upload Documents" />
                  </Form.Group>
                </li>
              </ul>
            </div>
          </div>
        </section>
        <section className="account_setup_right_section account_education_page_right_section">
          <div className="account_setup_right_content">
            <div className="page_comman_heading_section">
              <CommanHeading heading="Gap" />
              <p>
                Please explain any gaps in your work history or insurance below.
                Document all time since graduation.
              </p>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">Work History:</p>
              {workHistoryList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Gap Start Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="gapStartDate"
                          value={x.gapStartDate}
                          onChange={(e) => handleWorkHistoryListChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Gap End Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="gapEndDate"
                          value={x.gapEndDate}
                          onChange={(e) => handleWorkHistoryListChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Explanation:</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="explanation"
                          value={x.explanation}
                          onChange={(e) => handleWorkHistoryListChange(e, i)}
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button
                className="pluse_add_text_btn"
                onClick={handleWorkHistoryListAddClick}
              >
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
              </Button>
            </div>
            <div className="acount_setup_input_content home_address_form_content">
              <p className="acount_setup_input_small_heading">
                Insurance History:
              </p>
              {insuranceHistoryList.map((x, i) => {
                return (
                  <Row className="acount_setup_input_label_row">
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Gap Start Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="gapStartDate"
                          value={x.gapStartDate}
                          onChange={(e) =>
                            handleInsuranceHistoryListChange(e, i)
                          }
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={6}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Gap End Date:</Form.Label>
                        <Form.Control
                          type="date"
                          name="gapEndDate"
                          value={x.gapEndDate}
                          onChange={(e) =>
                            handleInsuranceHistoryListChange(e, i)
                          }
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={12}>
                      <Form.Group
                        className="acount_setup_input_label"
                        controlId="exampleForm.ControlInput1"
                      >
                        <Form.Label>Explanation:</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="explanation"
                          value={x.explanation}
                          onChange={(e) =>
                            handleInsuranceHistoryListChange(e, i)
                          }
                        />
                      </Form.Group>
                    </Col>
                  </Row>
                );
              })}
              <Button
                className="pluse_add_text_btn"
                onClick={handleInsuranceHistoryListAddClick}
              >
                <svg
                  width="14"
                  height="14"
                  viewBox="0 0 14 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M14 8H8V14H6V8H0V6H6V0H8V6H14V8Z" fill="black" />
                </svg>
              </Button>
              <div className="next_back_btns">
                <Link to={"/account-setup-work-history"} className="back_btn">
                  Back
                </Link>
                <Link
                  to={"/account-setup-peer-references"}
                  className="next_btn"
                >
                  Next
                </Link>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default AccountSetupGapScreen;
